using FluentAssertions;

namespace Cuentraletras.UnitTests;

public class ContadorTests
{
    [Fact]
    public void Given_Empty_Word_It_Should_Return_Zero()
    {
        // Given
        var contador = new Contador();
        var text = string.Empty;
        var letter = 'Z';

        // When
        var result = contador.GetCharCount(text, letter);

        // Then
        result.Should().Be(0);
    }

    [Fact]
    public void Given_Text_Not_Containing_Letter_It_Should_Return_Zero()
    {
        // Given
        var contador = new Contador();
        string text = "Hola que tal?";
        char letter = 'z';

        // When
        var result = contador.GetCharCount(text, letter);

        // Then
        result.Should().Be(0);
    }

    [Fact]
    public void Given_Text_Containing_One_Letter_It_Should_Return_One()
    {
        // Given
        var contador = new Contador();
        string text = "Hola que tal?";
        char letter = 'q';

        // When
        var result = contador.GetCharCount(text, letter);

        // Then
        result.Should().Be(1);
    }

    [Theory]
    [InlineData("hola", 'o', 1)]
    [InlineData("adios", 'a', 1)]
    [InlineData("", 'o', 0)]
    [InlineData("aprendiendo a testear con .NET", 'a', 3)]
    public void Given_Text_It_Should_Return_Expected_Count(
        string text,
        char letter,
        int expectedCount)
    {
        // Given
        var contador = new Contador();

        // When
        var result = contador.GetCharCount(text, letter);

        // Then
        result.Should().Be(expectedCount);
    }
}